package com.skoobalon.emu.gb.util;

/**
 * @author Steven Wallace
 */
public final class HexUtil {
    public static String toHexString8(int value) {
        return toHexString8(value, false);
    }

    public static String toHexString8(int value, boolean add0x) {
        return String.format((add0x ? "0x" : "") + "%02X", value);
    }

    public static String toHexString16(int value, boolean add0x) {
        return String.format((add0x ? "0x" : "") + "%04X", value);
    }

    public static String toHexString16(int value) {
        return toHexString16(value, false);
    }
}
