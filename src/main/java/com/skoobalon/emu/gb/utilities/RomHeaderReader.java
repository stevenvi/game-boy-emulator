package com.skoobalon.emu.gb.utilities;

import com.google.common.eventbus.EventBus;
import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.rom.GameBoyRom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author Steven Wallace
 */
public class RomHeaderReader {
    private static final Logger logger = LoggerFactory.getLogger(RomHeaderReader.class);

    public static void main(String[] args) {
        // Open first argument as filename
        try {
            GameBoyRom rom = new GameBoyRom(Files.readAllBytes(Paths.get(args[0])));
            Cpu cpu = new Cpu(rom, new Mmu(new EventBus()));
            for (int i = 0; i < 10; i++) {
                cpu.executeOpCode(cpu.getNextOpCode());
            }
        } catch (FileNotFoundException e) {
            logger.error("Cannot find file {}", args[0]);
        } catch (IOException e) {
            logger.error("Error reading input", e);
        }
    }
}
