package com.skoobalon.emu.gb.utilities.debugger.ui;

import javafx.scene.control.TableView;

/**
 * @author Steven Wallace
 */
public class StackView extends TableView {
//    public StackView() {
//        getColumns().addAll(
//                createColumn("Address", "address"),
//                createColumn("Value", "value")
//        );
////        setI
//    }
//
//    private TableColumn<StackWordHolder, String> createColumn(String name, String property) {
//
//    }

    public static class StackWordHolder {
        private String address;
        private int value;

        public StackWordHolder(String address, int value) {
            this.address = address;
            this.value = value;
        }


        public String getAddress() {
            return address;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }
}
