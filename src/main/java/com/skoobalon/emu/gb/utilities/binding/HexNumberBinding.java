package com.skoobalon.emu.gb.utilities.binding;

import javafx.beans.binding.StringBinding;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Steven Wallace
 */
public class HexNumberBinding extends StringBinding {

    private final boolean include0x;
    private final int digits;
    private final SimpleIntegerProperty dependency;

    public HexNumberBinding(SimpleIntegerProperty dependency, int digits, boolean include0x) {
        super.bind(dependency);
        this.dependency = dependency;
        this.digits = digits;
        this.include0x = include0x;
    }

    @Override
    public void dispose() {
        super.unbind(dependency);
    }

    @Override
    protected String computeValue() {
        return (include0x ? "0x" : "") + String.format("%0" + digits + "X", dependency.get());
    }

    @Override
//    @ReturnsUnmodifiableCollection
    public ObservableList<?> getDependencies() {
        return FXCollections.singletonObservableList(dependency);
    }
}
