package com.skoobalon.emu.gb.utilities.debugger.ui;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.skoobalon.emu.gb.hw.cpu.ops.Operation;
import com.skoobalon.emu.gb.rom.GameBoyRom;
import com.skoobalon.emu.gb.util.HexUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Observable;

/**
 * Assembler instructions view
 * @author Steven Wallace
 */
public class AsmView extends BorderPane {
    private static final Logger logger = LoggerFactory.getLogger(AsmView.class);

    private final Label label = new Label("ROM");
    private final HighlightableTableView table = new HighlightableTableView();

    private ObservableList<CpuOpHolder> ops = FXCollections.observableArrayList();
    private ImmutableMap<Integer, Integer> opRowByAddress;

    public AsmView() {
        table.getColumns().addAll(
                createColumn("Addr", "address"),
                createColumn("Op", "opcode"),
                createColumn("ASM", "asm"),
                createColumn("Cycles", "time")
        );

        table.setItems(ops);

        label.getStyleClass().add("viewLabel");
        setTop(label);
        setCenter(table);
    }

    private TableColumn<CpuOpHolder, String> createColumn(String name, String property) {
        TableColumn<CpuOpHolder, String> c = new TableColumn<>(name);
        c.setEditable(false);
        c.setSortable(false);
        c.setCellValueFactory(new PropertyValueFactory<>(property));
        c.getStyleClass().add("monospaced");
        return c;
    }

    public void setRom(GameBoyRom rom) {
        this.ops.clear();
        if (rom == null) {
            return;
        }

        final int romSize = 1024 * rom.getRomSize().getSizeInKb();
        List<CpuOpHolder> ops = Lists.newArrayListWithCapacity(romSize);
        ImmutableMap.Builder<Integer, Integer> opRowByAddressBuilder = ImmutableMap.builder();

        // Convert every single instruction starting at 0x100 into a CpuOpHolder object
        for (int i = 0x100; i < romSize; i++) {
            if (i == 0x104) {
                // Skip over header
                ops.add(new CpuOpHolder("0104-014F", "--", "HEADER", "--"));
                i = 0x150;
            }

            int addr = i;
            int opcode = rom.readByteAt(i);
            if (opcode == 0xCB) {
                opcode = (opcode << 8) | rom.readByteAt(++i);
            }

            Operation op = Operation.byOpCode(opcode);
            if (op == null) {
                // TODO: Add in something to signify that there's an invalid op in here
                logger.warn("Skipping unsupported opcode {} at {}", HexUtil.toHexString8(opcode, true), HexUtil.toHexString16(addr, true));
                continue;
            }

            String desc = op.getDescription();
            if (desc.contains("nnnn")) {
                desc = desc.replace("nnnn", HexUtil.toHexString16(rom.readWordAt(addr + 1)));
                i += 2;
            }
            if (desc.contains("nn")) {
                desc = desc.replace("nn", HexUtil.toHexString8(rom.readByteAt(addr + 1)));
                i += 1;
            }

            String cycles = op.getMinClockCycles() == op.getMaxClockCycles()
                    ? "" + op.getMinClockCycles()
                    : (op.getMinClockCycles() + "-" + op.getMaxClockCycles());

            CpuOpHolder opHolder = new CpuOpHolder(addr, opcode, desc, cycles);
            ops.add(opHolder);
            opRowByAddressBuilder.put(addr, ops.indexOf(opHolder));
        }

        this.ops.addAll(ops);
        this.opRowByAddress = opRowByAddressBuilder.build();

        setHighlighted(0x100);
    }

    public void setHighlighted(int address) {
        int row = opRowByAddress.get(address);
        table.highlightRow(row);
    }

    public static class CpuOpHolder extends Observable {
        private final String address;
        private final String opcode;
        private final String asm;
        private final String time;

        public CpuOpHolder(int address, int opcode, String asm, String time) {
            this.address = HexUtil.toHexString16(address, false);
            this.opcode = HexUtil.toHexString8(opcode, false);
            this.asm = asm;
            this.time = time;
        }

        public CpuOpHolder(String address, String opcode, String asm, String time) {
            this.address = address;
            this.opcode = opcode;
            this.asm = asm;
            this.time = time;
        }

        public String getAddress() {
            return address;
        }

        public String getOpcode() {
            return opcode;
        }

        public String getAsm() {
            return asm;
        }

        public String getTime() {
            return time;
        }
    }
}
