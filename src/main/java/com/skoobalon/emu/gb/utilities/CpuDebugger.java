package com.skoobalon.emu.gb.utilities;

import com.google.common.collect.Sets;
import com.skoobalon.emu.gb.hw.GameBoy;
import com.skoobalon.emu.gb.rom.GameBoyRom;
import com.skoobalon.emu.gb.utilities.debugger.ui.AsmView;
import com.skoobalon.emu.gb.utilities.debugger.ui.MemoryView;
import com.skoobalon.emu.gb.utilities.debugger.ui.RegisterView;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Steven Wallace
 */
public class CpuDebugger extends Application {
    private static final Logger logger = LoggerFactory.getLogger(CpuDebugger.class);

    private static GameBoy gameboy;

    /**
     * Contains each rom address to break execution on
     */
    private final Set<Integer> breakpoints = Sets.newHashSet(0x297);

    private AsmView asm;
    private MemoryView memory;
    private RegisterView registers;

    private ExecutorService executor = Executors.newSingleThreadExecutor();
    private RunTask runTask = null;

    public static void main(String[] args) throws Exception {
        gameboy = new GameBoy(new GameBoyRom(Files.readAllBytes(Paths.get(args[0]))));
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setOnCloseRequest(e -> {
            stopCpu();
            executor.shutdown();
        });

        BorderPane menuAndContentBox = new BorderPane();
        Scene scene = new Scene(menuAndContentBox);
        scene.getStylesheets().add(getClass().getClassLoader().getResource("css/general.css").toExternalForm());
        scene.getStylesheets().add(getClass().getClassLoader().getResource("css/highlightingTable.css").toExternalForm());

        BorderPane contentPane = new BorderPane();
        menuAndContentBox.setTop(createMenu(primaryStage));
        menuAndContentBox.setCenter(contentPane);

        primaryStage.setTitle("Game Boy CPU Debugger");
        primaryStage.setWidth(1024);
        primaryStage.setHeight(768);

        asm = new AsmView();
        asm.setRom(gameboy.getRom());
        resetCpu();

        memory = new MemoryView();
        memory.setMmu(gameboy.getMmu());

        registers = new RegisterView(gameboy.getCpu());

        // Listen for change events
        gameboy.getBus().register(memory);

        contentPane.setLeft(asm);
        contentPane.setCenter(memory);
        contentPane.setRight(registers);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private MenuBar createMenu(Stage primaryStage) {
        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("_File");
        fileMenu.getItems().addAll(
                createMenuItem("_Open...", new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN), e -> logger.error("Not yet implemented")),
                createMenuItem("E_xit", null, e -> primaryStage.close())
        );

        Menu debugMenu = new Menu("_Debug");
        debugMenu.getItems().addAll(
                createMenuItem("R_eset", new KeyCodeCombination(KeyCode.F2), e -> resetCpu()),
                createMenuItem("_Run", new KeyCodeCombination(KeyCode.F5), e -> resumeCpu()),
                createMenuItem("_Stop", new KeyCodeCombination(KeyCode.F6), e -> stopCpu()),
                createMenuItem("S_tep", new KeyCodeCombination(KeyCode.F7), e -> stepCpu())
        );
//        debugMenu.getItems().get(1).disableProperty().bind(Bindings.isNull(runThread));
//        debugMenu.getItems().get(2).disableProperty().bind(Bindings.isNotNull(runThread));

        menuBar.getMenus().addAll(fileMenu, debugMenu);
        return menuBar;
    }

    private MenuItem createMenuItem(String label, KeyCodeCombination accelerator, EventHandler<ActionEvent> action) {
        MenuItem menuItem = new MenuItem(label);
        menuItem.setOnAction(action);
        menuItem.setAccelerator(accelerator);
        return menuItem;
    }

    private void resetCpu() {
        gameboy.getCpu().reset();
        asm.setHighlighted(gameboy.getCpu().getRegisterPC().getValue());
    }

    private void resumeCpu() {
        runTask = new RunTask(breakpoints);
        executor.submit(runTask);
    }

    private void stopCpu() {
        if (runTask != null) {
            runTask.stopRunning();
            runTask = null;
            asm.setHighlighted(gameboy.getCpu().getNextOpCode());
            gameboy.getCpu().getRegisterPC().decrementAndGet();
        }
    }

    private void stepCpu() {
        gameboy.getCpu().executeOpCode(gameboy.getCpu().getNextOpCode());
        asm.setHighlighted(gameboy.getCpu().getRegisterPC().get());
    }

    private static class RunTask extends Task {
        private final Set<Integer> breakpoints;

        private boolean running = true;

        public RunTask(Set<Integer> breakpoints) {
            this.breakpoints = breakpoints;
        }

        @Override
        public Object call() {
            while (running) {
                if (breakpoints.contains(gameboy.getCpu().getRegisterPC().get())) {
                    stopRunning();
                    logger.info("Breakpoint hit");
                }
                gameboy.getCpu().executeOpCode(gameboy.getCpu().getNextOpCode());
            }
            return null;
        }

        public void stopRunning() {
            running = false;
        }

    }
}
