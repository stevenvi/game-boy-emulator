package com.skoobalon.emu.gb.utilities.debugger.ui;

import javafx.scene.control.TableView;

/**
 * @author Steven Wallace
 */
public class HighlightableTableView<T> extends TableView<T> {

    private static final int SCROLL_THRESHOLD = 10;

    private final StyleChangingRowFactory<T> rowFactory = new StyleChangingRowFactory<T>("highlighted");
    private int lastHighlightedIndex = Integer.MAX_VALUE;

    public HighlightableTableView() {
        super();
        setRowFactory(rowFactory);
    }

    public void highlightRow(int row) {
        highlightRow(row, true);
    }

    public void highlightRow(int row, boolean centerOnHighlight) {
        rowFactory.getStyledRowIndices().clear();
        rowFactory.getStyledRowIndices().setAll(row);

        if (centerOnHighlight) {
            // Center this row if we start to get close to the edge of the screen
            // TODO: This can surely be done better...
            if (Math.abs(lastHighlightedIndex - row) >= SCROLL_THRESHOLD) {
                scrollTo(row - 5);
            }
        }
        lastHighlightedIndex = row;
    }
}
