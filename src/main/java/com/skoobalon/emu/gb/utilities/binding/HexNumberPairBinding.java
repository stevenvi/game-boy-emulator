package com.skoobalon.emu.gb.utilities.binding;

import javafx.beans.binding.StringBinding;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Steven Wallace
 */
public class HexNumberPairBinding extends StringBinding {

    private final SimpleIntegerProperty byte0;
    private final SimpleIntegerProperty byte1;

    public HexNumberPairBinding(SimpleIntegerProperty byte0, SimpleIntegerProperty byte1) {
        super.bind(byte0, byte1);
        this.byte0 = byte0;
        this.byte1 = byte1;
    }

    @Override
    public void dispose() {
        super.unbind(byte0, byte1);
    }

    @Override
    protected String computeValue() {
        return String.format("%02X%02X", byte0.get(), byte1.get());
    }

    @Override
//    @ReturnsUnmodifiableCollection
    public ObservableList<?> getDependencies() {
        return FXCollections.observableArrayList(byte0, byte1);
    }
}
