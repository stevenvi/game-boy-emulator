package com.skoobalon.emu.gb.utilities.debugger.ui;

import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import com.skoobalon.emu.gb.hw.MemoryChangedEvent;
import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.util.HexUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Steven Wallace
 */
public class MemoryView extends BorderPane {
    private static final Logger logger = LoggerFactory.getLogger(MemoryView.class);

    private final Label label = new Label("Memory");
    private final HighlightableTableView table = new HighlightableTableView();

    private Mmu mmu;
    private ObservableList<MemoryDataHolder> data = FXCollections.observableArrayList();

    public MemoryView() {
        setTop(label);
        setCenter(table);

        table.getColumns().addAll(
                createColumn("Address", "address"),
                createColumn("Value", "memory")
        );

        table.setItems(data);
    }

    public void setMmu(Mmu mmu) {
        this.mmu = mmu;
        data.clear();

        for (int addr = 0; addr < 1024 * 64; addr += 16) {
            // Get the next strip
            data.add(new MemoryDataHolder(addr, getMemoryStrip(addr)));
        }
    }

    private TableColumn<MemoryDataHolder, String> createColumn(String name, String property) {
        TableColumn<MemoryDataHolder, String> c = new TableColumn<>(name);
        c.setEditable(false);
        c.setSortable(false);
        c.setCellValueFactory(new PropertyValueFactory<>(property));
        c.getStyleClass().add("monospaced");
        return c;
    }

    private List<Byte> getMemoryStrip(int startAddress) {
        // Read all 64k in 16 byte strips
        final int stripSize = 16;
        List<Byte> list = Lists.newArrayList();
        for (int i = startAddress; i < startAddress + stripSize; i++) {
            list.add((byte)mmu.read8(i));
        }
        return list;
    }

    @Subscribe
    public void onMemoryChanged(MemoryChangedEvent event) {
        // Determine the row that was affected
        int startAddress = event.getAddress() & 0xFFF0;
        int row = startAddress >> 4;
        data.get(row).setMemory(getMemoryStrip(startAddress));
        table.highlightRow(row);
    }


    public static class MemoryDataHolder {
        private String address;
        private String memory;

        public MemoryDataHolder(int address, List<Byte> memory) {
            this.address = HexUtil.toHexString16(address);
            setMemory(memory);
        }

        public String getAddress() {
            return address;
        }

        public String getMemory() {
            return memory;
        }

        public void setMemory(List<Byte> memory) {
            StringBuilder sb = new StringBuilder();
            for (Byte b : memory) {
                sb.append(HexUtil.toHexString8(b & 0xFF));
                sb.append(" ");
            }
            this.memory = sb.toString();
        }
    }
}
