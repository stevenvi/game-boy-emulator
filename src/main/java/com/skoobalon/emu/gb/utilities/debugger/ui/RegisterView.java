package com.skoobalon.emu.gb.utilities.debugger.ui;

import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.utilities.binding.HexNumberBinding;
import com.skoobalon.emu.gb.utilities.binding.HexNumberPairBinding;
import javafx.beans.binding.StringBinding;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

/**
 * @author Steven Wallace
 */
public class RegisterView extends BorderPane {

    private final Label label = new Label("Registers");
    private final TableView table = new TableView<RegisterPairHolder>();

    private Cpu cpu;

    public RegisterView(Cpu cpu) {
        this.cpu = cpu;

        label.getStyleClass().add("viewLabel");
        setTop(label);

//        Callback<TableColumn.CellDataFeatures<RegisterPairHolder, HexNumberBinding>, HexNumberBinding> yy = rph -> rph.getValue().getR0();
        Callback<TableColumn.CellDataFeatures<RegisterPairHolder, StringBinding>, StringBinding> xx = rph -> rph.getValue().getBinding();

        table.getColumns().addAll(
                createColumn("", new PropertyValueFactory<>("name")),
                createColumn("", xx)
        );
        table.setEditable(false);

        table.getItems().addAll(
                new RegisterPairHolder("AF", new HexNumberPairBinding(cpu.getRegisterA(), cpu.getRegisterF())),
                new RegisterPairHolder("BC", new HexNumberPairBinding(cpu.getRegisterB(), cpu.getRegisterC())),
                new RegisterPairHolder("DE", new HexNumberPairBinding(cpu.getRegisterD(), cpu.getRegisterE())),
                new RegisterPairHolder("HL", new HexNumberPairBinding(cpu.getRegisterH(), cpu.getRegisterL())),
                new RegisterPairHolder("SP", new HexNumberBinding(cpu.getRegisterSP(), 4, false)),
                new RegisterPairHolder("PC", new HexNumberBinding(cpu.getRegisterPC(), 4, false))
        );
        setCenter(table);
    }

    private TableColumn<RegisterPairHolder, ?> createColumn(String name, Callback cellValueFactory) {
        TableColumn<RegisterPairHolder, ?> c = new TableColumn<>(name);
        c.getStyleClass().add("monospaced");
        c.setCellValueFactory(cellValueFactory);
        c.setEditable(false);
        c.setSortable(false);
        return c;
    }

//    public void setCpu(Cpu cpu) {
//        this.cpu = cpu;
//    }

    public static class RegisterPairHolder {
        private String name;
        private StringBinding binding;

        public RegisterPairHolder(String name, StringBinding binding) {
            this.name = name;
            this.binding = binding;
        }

        public String getName() {
            return name;
        }

        public StringBinding getBinding() {
            return binding;
        }
    }
}
