package com.skoobalon.emu.gb.rom;

/**
 * @author Steven Wallace
 */
public enum CountryCode {
    Japan(0),
    NonJapan(1);

    public static CountryCode byId(int id) {
        switch (id) {
            case 0: return Japan;
            case 1: return NonJapan;
            default: return null;
        }
    }

    private final byte id;

    CountryCode(int id) {
        this.id = (byte)id;
    }

    public byte getId() {
        return id;
    }
}
