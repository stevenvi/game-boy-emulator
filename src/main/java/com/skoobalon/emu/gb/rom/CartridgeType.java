package com.skoobalon.emu.gb.rom;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

/**
 * @author Steven Wallace
 */
public enum CartridgeType {
    ROM(0x00, "ROM only"),
    MBC1(0x01, "MBC1"),
    MBC1_RAM(0x02, "MBC1+RAM", CartridgeFeature.RAM),
    MBC1_RAM_BATTERY(0x03, "MBC1+RAM+BATTERY", CartridgeFeature.RAM, CartridgeFeature.Battery),
    MBC2(0x05, "MBC2"),
    MBC2_BATTERY(0x06, "MBC2+BATTERY", CartridgeFeature.Battery),
    ROM_RAM(0x08, "ROM+RAM", CartridgeFeature.RAM),
    ROM_RAM_BATTERY(0x09, "ROM+RAM+BATTERY", CartridgeFeature.RAM, CartridgeFeature.Battery),
    MMM01(0x0B, "MMM01"),
    MMM01_RAM(0x0C, "MMM01+RAM", CartridgeFeature.RAM),
    MMM01_RAM_BATTERY(0x0D, "MMM01+RAM+BATTERY", CartridgeFeature.RAM, CartridgeFeature.Battery),
    MBC3_TIMER_BATTERY(0x0F, "MC3+TIMER+BATTERY", CartridgeFeature.Timer, CartridgeFeature.Battery),
    MBC3_TIMER_RAM_BATTERY(0x10, "MBC3+TIMER+RAM+BATTERY", CartridgeFeature.Timer, CartridgeFeature.RAM, CartridgeFeature.Battery),
    MBC3(0x11, "MBC3"),
    MBC3_RAM(0x12, "MBC3+RAM", CartridgeFeature.RAM),
    MBC3_RAM_BATTERY(0x13, "MBC3+RAM+BATTERY", CartridgeFeature.RAM, CartridgeFeature.Battery),
    MBC4(0x15, "MBC5"),
    MBC4_RAM(0x16, "MBC4+RAM", CartridgeFeature.RAM),
    MBC4_RAM_BATTERY(0x17, "MBC4+RAM+BATTERY", CartridgeFeature.RAM, CartridgeFeature.Battery),
    MBC5(0x19, "MBC5"),
    MBC5_RAM(0x1A, "MBC5+RAM", CartridgeFeature.RAM),
    MBC5_RAM_BATTERY(0x1B, "MBC5+RAM+BATTERY", CartridgeFeature.RAM, CartridgeFeature.Battery),
    MBC5_RUMBLE(0x1C, "MBC5+RUMBLE", CartridgeFeature.Rumble),
    MBC5_RUMBLE_RAM(0x1D, "MBC5+RUMBLE+RAM", CartridgeFeature.Rumble, CartridgeFeature.RAM),
    MBC5_RUMBLE_RAM_BATTERY(0x1E, "MBC5+RUMBLE+RAM+BATTERY", CartridgeFeature.Rumble, CartridgeFeature.RAM, CartridgeFeature.Battery),
    POCKET_CAMERA(0xFC, "Pocket Camera"),
    BandaiTAMA5(0xFD, "Bandai TAMA5"),
    HuC3(0xFE, "HuC3"),
    HuC1_RAM_BATTERY(0xFF, "HuC1+RAM+BATTERY", CartridgeFeature.RAM, CartridgeFeature.Battery);

    private static final ImmutableMap<Byte, CartridgeType> byIdMap;
    static {
        ImmutableMap.Builder<Byte, CartridgeType> builder = ImmutableMap.builder();
        for (CartridgeType type : values()) {
            builder.put(type.getId(), type);
        }
        byIdMap = builder.build();
    }

    public static CartridgeType byId(int id) {
        return byIdMap.get((byte)id);
    }

    private final byte id;
    private final String description;
    private final ImmutableSet<CartridgeFeature> features;

    CartridgeType(int id, String description, CartridgeFeature... features) {
        this.id = (byte)id;
        this.description = description;
        this.features = ImmutableSet.copyOf(features);
    }

    public byte getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public boolean hasFeature(CartridgeFeature feature) {
        return features.contains(feature);
    }

}
