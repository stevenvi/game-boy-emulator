package com.skoobalon.emu.gb.rom;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * http://gameboy.mongenel.com/dmg/gbspec.txt
 * @author Steven Wallace
 */
public class GameBoyRom {
    private static final Logger logger = LoggerFactory.getLogger(GameBoyRom.class);

    /**
     * Jump table for different RST instructions
     */
    private final Map<Integer, Integer> rstJumpTable;

    private final int vBlankStartAddr;
    private final int lcdcStatusStartAddr;
    private final int timerOverflowStartAddr;
    private final int serialXferCompleteStartAddr;

    // TODO: p10-p13 interrupt start addrs

    private final String name;
    private final Licensee licensee;
    private final boolean color;
    private final boolean superGameBoy;
    private final CartridgeType type;
    private final RomSize romSize;
//    private final SaveRamSize sramSize;
    private final CountryCode countryCode;

    private byte[] rom;

    public GameBoyRom(byte[] rom) throws IOException {
        // Store the rom for safekeeping
        this.rom = rom;


        // ----------------------------------------------------------------
        // 0x0000 - 0x0039 RST jump addresses

        ImmutableMap.Builder<Integer, Integer> rstBuilder = ImmutableMap.builder();
        int[] rstAddrs = new int[]{ 0x0, 0x8, 0x10, 0x18, 0x20, 0x28, 0x30, 0x38 };
        for (int addr : rstAddrs) {
            rstBuilder.put(addr, readWordAt(addr));
        }
        rstJumpTable = rstBuilder.build();

        // ----------------------------------------------------------------
        // 0x0040 - 0x0067 Interrupt start addresses

        vBlankStartAddr = readWordAt(0x40);
        lcdcStatusStartAddr = readWordAt(0x48);
        timerOverflowStartAddr = readWordAt(0x50);
        serialXferCompleteStartAddr = readWordAt(0x58);

        // ----------------------------------------------------------------
        // 0x0100 - 0x103 Begin code execution

        // Print for informational purposes only
        logger.info("First 4 instructions are: {}", DatatypeConverter.printHexBinary(readBytesAt(0x0100, 4)));

        // ----------------------------------------------------------------
        // 0x0104 - 0x0133 Scrolling "Nintendo" graphic

        // Verify that this is correct -- helps to validate a legit ROM
        // TODO: Would like to examine what these opcodes do :)
        int[] expectedNintendoLogo = new int[] {
                0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B,
                0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
                0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E,
                0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
                0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC,
                0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E
        };
        byte[] nintendoLogoFromRom = readBytesAt(0x0104, 48);
        for (int i = 0; i < expectedNintendoLogo.length; i++) {
            if ((byte)expectedNintendoLogo[i] != nintendoLogoFromRom[i]) {
                throw new IOException("Invalid Nintendo logo in ROM at position " + i);
            }
        }

        // ----------------------------------------------------------------
        // 0x0134 - 0x0142 Game title

        name = new String(readBytesAt(0x0134, 15), "UTF-8");
        logger.info("ROM name: '{}'", name);

        // ----------------------------------------------------------------
        // 0x0143 GBC flag

        color = readByteAt(0x0143) == 0x80;
        logger.info("Color cartridge: {}", color);

        // ----------------------------------------------------------------
        // 0x0144 - 0x0145 New licensee code

        int newLicenseeId = readWordAt(0x144);
        logger.info("New licensee id: {}", newLicenseeId);

        // ----------------------------------------------------------------
        // 0x0146 Super Game Boy feature flag

        superGameBoy = readByteAt( 0x0146) == 0x03;
        logger.info("Super GameBoy features: {}", superGameBoy);

        // ----------------------------------------------------------------
        // 0x0147 Cartridge type

        int cartTypeId = readByteAt(0x0147);
        this.type = CartridgeType.byId(cartTypeId);
        logger.info("Cartridge type id: {}; description: {}", cartTypeId, type == null ? "Unknown" : type.getDescription());

        // ----------------------------------------------------------------
        // 0x0148 Read ROM size

        int romSizeId = readByteAt(0x0148);
        romSize = RomSize.byId(romSizeId);
        logger.info("ROM size id: {}; kb: {}", romSizeId, romSize == null ? "Unknown" : romSize.getSizeInKb());

        // ----------------------------------------------------------------
        // 0x0149 Save RAM size

        int sramSizeId = readByteAt(0x0149);
        logger.info("SRAM size id: {}", sramSizeId);
        // TODO: Convert to enum

        // ----------------------------------------------------------------
        // 0x014A country code

        countryCode = CountryCode.byId(readByteAt(0x014A));
        logger.info("Country Code: {}", countryCode);

        // ----------------------------------------------------------------
        // 0x014B Old licensee code

        int oldLicenseeId = readByteAt(0x014B);
        logger.info("Old licensee id: {}", oldLicenseeId);
        licensee = Licensee.byIds(oldLicenseeId, newLicenseeId);
        logger.info("Licensee: {}", licensee);

        // ----------------------------------------------------------------
        // 0x014C Mask ROM version number (usually 0x00)

        int romVersion = readByteAt(0x014C);
        logger.info("ROM version: {}", romVersion);

        // ----------------------------------------------------------------
        // 0x014D Complement check
        // TODO: ???

        int complementCheck = readByteAt(0x014D);
        logger.info("Complement check value: {}", complementCheck);

        // ----------------------------------------------------------------
        // 0x014E-0x014F Checksum
        // This is the sum of all bytes in the ROM, except for these two bytes, taking the two lower bytes
        // GB ignores this value

        int globalChecksum = readWordAt(0x014E);
        logger.info("Global checksum: {}", globalChecksum);
        // TODO: Validate?
    }

    public int readByteAt(int position) {
        return rom[position] & 0xFF;
    }
    public int readWordAt(int position) {
        return (readByteAt(position + 1) << 8) | readByteAt(position);
    }

    private byte[] readBytesAt(int position, int length) {
        return Arrays.copyOfRange(rom, position, position + length);
    }

    public RomSize getRomSize() {
        return romSize;
    }

    /**
     * Returns a writable reference to the rom
     * Please be safe with this!
     * @return
     */
    public byte[] getRom() {
        return rom;
    }

}
