package com.skoobalon.emu.gb.rom;

/**
 * @author Steven Wallace
 */
public enum CartridgeFeature {
    RAM,
    Battery,
    Timer,
    Rumble
}
