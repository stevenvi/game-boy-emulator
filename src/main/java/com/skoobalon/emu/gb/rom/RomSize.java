package com.skoobalon.emu.gb.rom;

import com.google.common.collect.ImmutableMap;

/**
 * @author Steven Wallace
 */
public enum RomSize {
    k32(0x00, 32),
    k64(0x01, 64),
    k128(0x02, 128),
    k256(0x03, 256),
    k512(0x04, 512),
    k1024(0x05, 1024),
    k2048(0x06, 2048),
    k4096(0x07, 4096);

    private final byte id;
    private final int sizeInKb;

    private static final ImmutableMap<Byte, RomSize> byId;
    private static final ImmutableMap<Integer, RomSize> bySizeInKb;
    static {
        ImmutableMap.Builder<Byte, RomSize> byIdBuilder = ImmutableMap.builder();
        ImmutableMap.Builder<Integer, RomSize> bySizeInKbBuilder = ImmutableMap.builder();
        for (RomSize rs : values()) {
            byIdBuilder.put(rs.getId(), rs);
            bySizeInKbBuilder.put(rs.getSizeInKb(), rs);
        }
        byId = byIdBuilder.build();
        bySizeInKb = bySizeInKbBuilder.build();
    }

    public static RomSize byId(int id) {
        return byId.get((byte)id);
    }

    public static RomSize bySizeInKb(int sizeInKb) {
        return bySizeInKb.get(sizeInKb);
    }

    RomSize(int id, int sizeInKb) {
        this.id = (byte)id;
        this.sizeInKb = sizeInKb;
    }

    public byte getId() {
        return id;
    }

    public int getSizeInKb() {
        return sizeInKb;
    }
}
