package com.skoobalon.emu.gb.hw.cpu;

/**
 * A 16-bit register
 * @author Steven Wallace
 */
public class Register16 extends Register {

    @Override
    public void set(int value) {
        super.set(value & 0xFFFF);
    }

    @Override
    public String toString() {
        return String.format("%04X", get());
    }
}
