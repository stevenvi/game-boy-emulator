package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Register16;
import com.skoobalon.emu.gb.hw.cpu.Register8;

/**
 * Increment
 * @author Steven Wallace
 */
public class INC {
    /**
     * OpCodes: 0x03, 0x13, 0x23
     * @param cpu
     * @param r0
     * @param r1
     */
    public static void rr(Cpu cpu, Register8 r0, Register8 r1) {
        r1.set(r1.getValue() + 1);
        if (r1.getValue() == 0) {
            r0.set(r0.getValue() + 1);
        }
    }

    /**
     * OpCodes: 0x04, 0x0C, 0x14, 0x1C, 0x24, 0x2C, 0x3C
     * @param cpu
     * @param register
     */
    public static void r(Cpu cpu, Register8 register) {
        // Increment register
        register.set(register.getValue() + 1);

        // Update flags
        cpu.getRegisterF().setZero(register.getValue() == 0);
    }

    /**
     * Increments a 16-bit register
     *
     * OpCodes: 0x33
     * @param cpu
     * @param register
     */
    public static void R(Cpu cpu, Register16 register) {
        register.set(register.getValue() + 1);
    }

    /**
     * Increment value in memory pointed to by input registers
     * Treats the register pair as a 16-bit word and increment value in memory location represented by that value,
     * setting the Zero flag if applicable.
     *
     * OpCodes: 0x34
     * @param cpu
     * @param mmu
     * @param r0
     * @param r1
     */
    public static void rr(Cpu cpu, Mmu mmu, Register8 r0, Register8 r1) {
        int addr = (r0.getValue() << 8) + r1.getValue();
        int i = mmu.read8(addr) + 1;
        mmu.write8(addr, i);
        cpu.getRegisterF().setZero(i == 0);
    }
}
