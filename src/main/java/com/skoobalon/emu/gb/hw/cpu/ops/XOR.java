package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Register8;

/**
 * @author Steven Wallace
 */
public class XOR {
    /**
     * Binary XOR between values in src and dest registers, stored in dest
     *
     * Flags: Zero if result is zero
     * m-Time: 1
     * OpCodes: 0xA8-0xAD, 0xAF
     * @param cpu
     * @param dest
     * @param src
     */
    public static void r_r(Cpu cpu, Register8 dest, Register8 src) {
        dest.set(dest.getValue() ^ src.getValue());
        cpu.getRegisterF().setZero(dest.getValue() == 0);
    }

    /**
     * Binary XOR between value in memory pointed to by src registers and dest register, stored in dest
     *
     * Flags: Zero if result is zero
     * m-Time: 2
     * OpCodes: 0xAE
     *
     * @param cpu
     * @param mmu
     * @param dest
     * @param src0
     * @param src1
     */
    public static void r_rr(Cpu cpu, Mmu mmu, Register8 dest, Register8 src0, Register8 src1) {
        dest.set(dest.getValue() ^ mmu.read8(src0, src1));
        cpu.getRegisterF().setZero(dest.getValue() == 0);
    }

    /**
     * OpCodes: 0xEE
     * @param cpu
     * @param mmu
     * @param dest
     */
    public static void n(Cpu cpu, Mmu mmu, Register8 dest) {
        int pc = cpu.getRegisterPC().getValue();
        int newValue = dest.getValue() ^ mmu.read8(pc);
        dest.set(newValue);
        cpu.getRegisterPC().set(pc + 1);
        cpu.getRegisterF().setZero(newValue == 0);
    }
}
