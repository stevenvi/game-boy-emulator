package com.skoobalon.emu.gb.hw;

import com.google.common.eventbus.EventBus;
import com.skoobalon.emu.gb.hw.cpu.Register8;
import com.skoobalon.emu.gb.rom.GameBoyRom;

import java.util.Arrays;

/**
 * Game Boy has 64 KB of addressable memory, allocated for a bunch of different purposes...
 * This is one of those times where you miss C
 *
 * @author Steven Wallace
 */
public class Mmu {

    private final EventBus bus;
    private GameBoyRom rom;

    private byte[] romBank0;    // 0x0000-0x3FFF
    private byte[] romBank1;    // 0x4000-0x7FFF
    private byte[] vRam;        // 0x8000-0x9FFF
    private byte[] extRam;      // 0xA000-0xBFFF
    private byte[] workingRam;  // 0xC000-0xDFFF, also shadowed at 0xE000-0xFDFF
    private byte[] sprite;      // 0xFE00-0xFE9F
    private byte[] io;          // 0xFF00-0xFF7F
    private byte[] zeroPage;    // 0xFF80-0xFFFF

    public Mmu(EventBus bus) {
        this.bus = bus;

        // Create each chunk of memory
        romBank0 = new byte[16 * 1024];
        romBank1 = new byte[16 * 1024];
        vRam = new byte[8 * 1024];
        extRam = new byte[8 * 1024];
        workingRam = new byte[8 * 1024];
        sprite = new byte[160];
        io = new byte[128];
        zeroPage = new byte[128];
    }

    public void setRom(GameBoyRom rom) {
        this.rom = rom;
        romBank0 = Arrays.copyOfRange(rom.getRom(), 0x0000, 0x4000);
        romBank1 = Arrays.copyOfRange(rom.getRom(), 0x4000, 0x8000);
    }

    public int read8(Register8 addr0, Register8 addr1) {
        return read8((addr0.getValue() << 8) + addr1.getValue());
    }

    public int read8(int addr) {
        switch (addr & 0xF000) {
            case 0x0000:
            case 0x1000:
            case 0x2000:
            case 0x3000:
                return romBank0[addr];

            case 0x4000:
            case 0x5000:
            case 0x6000:
            case 0x7000:
                return romBank1[addr - 0x4000];

            case 0x8000:
            case 0x9000:
                return vRam[addr - 0x8000];

            case 0xA000:
            case 0xB000:
                return extRam[addr - 0xA000];

            case 0xC000:
            case 0xD000:
                return workingRam[addr - 0xC000];

            case 0xE000:
                // (Shadow RAM)
                return workingRam[addr - 0xE000];

            case 0xF000:
                // (Shadow RAM)
                // TODO: Handle sprite, io, and zeropage
                return workingRam[addr - 0xE000];

            default:
                // (This isn't actually possible)
                throw new RuntimeException("Invalid memory address requested: " + addr);
        }
    }
    
    public int read16(int addr) {
        return (read8(addr) << 8) | read8(addr + 1);
    }

    public void write8(Register8 addr0, Register8 addr1, int value) {
        write8((addr0.getValue() << 8) + addr1.getValue(), value);
    }

    public byte write8(int addr, int value) {
        byte v = (byte) value;
        bus.post(new MemoryChangedEvent(addr, read8(addr), value));

        switch (addr & 0xF000) {
            case 0x0000:
            case 0x1000:
            case 0x2000:
            case 0x3000:
                return romBank0[addr] = v;

            case 0x4000:
            case 0x5000:
            case 0x6000:
            case 0x7000:
                return romBank1[addr - 0x4000] = v;

            case 0x8000:
            case 0x9000:
                return vRam[addr - 0x8000] = v;

            case 0xA000:
            case 0xB000:
                return extRam[addr - 0xA000] = v;

            case 0xC000:
            case 0xD000:
                return workingRam[addr - 0xC000] = v;

            case 0xE000:
                // (Shadow RAM)
                return workingRam[addr - 0xE000] = v;

            case 0xF000:
                // (Shadow RAM)
                // TODO: Handle sprite, io, and zeropage
                return workingRam[addr - 0xE000] = v;

            default:
                // (This isn't actually possible)
                throw new RuntimeException("Invalid memory address requested: " + addr);
        }
    }
    
    public void write16(int addr, int value) {
        write8(addr, value >> 8);
        write8(addr + 1, value);
    }

}
