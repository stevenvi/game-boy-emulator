package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Flag;
import com.skoobalon.emu.gb.hw.cpu.Register8;

/**
 * @author Steven Wallace
 */
public class ADC {
    /**
     * Add and increment if previous operation resulted in a carry
     * Add value from source register into destination register, incrementing by 1 if previous operation set the
     * Carry flag.
     *
     * Flags: Zero if result is a zero (due to overflow), Carry if overflow occurs, other flags are unset
     * m-Time: 1
     * OpCodes: 0x88-0x8D, 0x8F
     * @param cpu
     * @param dest
     * @param src
     */
    public static void r_r(Cpu cpu, Register8 dest, Register8 src) {
        // Perform ADD operation, incrementing by 1 if previous operation overflowed
        int srcValue = src.getValue() + (cpu.getRegisterF().isSet(Flag.Carry) ? 1 : 0);
        dest.set(srcValue + dest.getValue());

        // Update flags and record time
        cpu.getRegisterF().setFlags(dest.getValue() < src.getValue(), false, false, dest.getValue() == 0);
    }

    /**
     * Add and increment if previous operation resulted in a carry
     * Adds the value in memory pointed to by the source registers into the value in the destination register,
     * incrementing by 1 if the previous operation set the Carry flag.
     *
     * Flags: Zero if result is a zero (due to overflow), Carry if overflow occurs, other flags are unset
     * m-Time: 2
     * OpCodes: 0x8E
     * @param cpu
     * @param mmu
     * @param dest
     * @param src0
     * @param src1
     */
    public static void r_rr(Cpu cpu, Mmu mmu, Register8 dest, Register8 src0, Register8 src1) {
        // Add value in memory at source registers with value in destination register
        int srcValue = mmu.read8(src0, src1) + dest.getValue();
        // Increment by 1 if the carry flag is set
        srcValue += (cpu.getRegisterF().isSet(Flag.Carry) ? 1 : 0);

        dest.set(srcValue + dest.getValue());

        // Update flags and record time
        cpu.getRegisterF().setFlags(dest.getValue() < srcValue, false, false, dest.getValue() == 0);
    }

    /**
     * OpCodes: 0xCE
     * @param cpu
     * @param mmu
     * @param dest
     */
    public static void r_n(Cpu cpu, Register8 dest) {
//        int tempValue = cpu.getNextOpCode();
//        int tempRegister = (dest.getValue() + tempValue + (cpu.getRegisterF().isSet(Flag.Carry) ? 1 : 0));
//        cpu.getRegisterF().setFlags((tempRegister & 0xFF) > 0, xx, false, );
//
//        AF.B.B0= (tempRegister.B.B1?C_FLAG:0)|ZeroTable[tempRegister.B.B0]|
//                ((AF.B.B1^tempValue^tempRegister.B.B0)&0x10?H_FLAG:0);
//        AF.B.B1=tempRegister.B.B0;


        int pc = cpu.getRegisterPC().getValue();
        int newValue = dest.getValue() + pc + (cpu.getRegisterF().isSet(Flag.Carry) ? 1 : 0);

        dest.set(newValue);
        cpu.getRegisterPC().set(pc + 1);
        cpu.getRegisterF().setFlags(newValue > 0xFF, false, false, newValue == 0);
    }
}
