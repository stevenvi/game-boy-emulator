package com.skoobalon.emu.gb.hw.cpu;

import javafx.beans.property.SimpleIntegerProperty;

/**
 * Shift right
 * @author Steven Wallace
 */
public abstract class Register extends SimpleIntegerProperty {

    public void setBit(int bit, boolean state) {
        if (state) {
            set(get() | (0x1 << bit));
        } else {
            set(get() & ~(0x1 << bit));
        }
    }

    public int getAndIncrement() {
        int value = get();
        set(value + 1);
        return value;
    }

    public int getAndDecrement() {
        int value = get();
        set(value - 1);
        return value;
    }

    public int incrementAndGet() {
        int value = get() + 1;
        set(value);
        return value;
    }

    public int decrementAndGet() {
        int value = get() - 1;
        set(value);
        return value;
    }

    /**
     * Shifts value right by 1 bit
     */
    public void rightShift() {
        set(get() >> 1);
    }

    /**
     * Shifts value left by 1 bit
     */
    public void leftShift() {
        set(get() << 1);
    }

}
