package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Register8;

/**
 * @author Steven Wallace
 */
public class SUB {
    /**
     * Subtract one register from another
     * The value in the destination register is decremented by the value in the source register
     *
     * Flags: Zero if result is zero, Operation, and Carry if underflow occurred
     * m-Time: 1
     * OpCodes: 0x90-0x95, 0x97
     * @param cpu
     * @param dest
     * @param src
     */
    public static void r_r(Cpu cpu, Register8 dest, Register8 src) {
        // Perform SUB operation
        dest.set(dest.getValue() - src.getValue());

        // Update flags and record time spent
        cpu.getRegisterF().setFlags(dest.getValue() > src.getValue(), false, true, dest.getValue() == 0);
    }

    /**
     * Subtract memory value from destination register
     * The value in memory pointed to by the source registers are decremented from and stored into the value in the
     * destination register.
     *
     * Flags: Zero if result is zero, Operation, and Carry if underflow occurred
     * m-Time: 2
     * OpCodes: 0x96
     *
     * @param cpu
     * @param mmu
     * @param dest
     * @param src0
     * @param src1
     */
    public static void r_rr(Cpu cpu, Mmu mmu, Register8 dest, Register8 src0, Register8 src1) {
        // Decrement destination register by memory value pointed to by source registers
        int newValue = dest.getValue() - mmu.read8(src0, src1);
        dest.set(newValue);

        // Update flags register and record time spent
        cpu.getRegisterF().setFlags(newValue < 0, false, true, newValue == 0);
    }

    /**
     * Subtract the next byte in the ROM from the destination register
     * OpCodes: 0xD6
     * @param cpu
     * @param dest
     */
    public static void r_n(Cpu cpu, Register8 dest) {
        // SUB NN
        int sub = cpu.getNextOpCode();
        int oldValue = dest.getValue();
        int newValue = oldValue - sub;
        dest.set(newValue);
        cpu.getRegisterF().setFlags(
                newValue < 0,
                ((oldValue ^ sub ^ dest.getValue()) & 0x10) != 0,
                true,
                dest.getValue() == 0
        );
    }
}
