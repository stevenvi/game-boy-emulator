package com.skoobalon.emu.gb.hw.cpu;

/**
 * An 8-bit register
 * @author Steven Wallace
 */
public class Register8 extends Register {

    @Override
    public void set(int value) {
        super.set(value & 0xFF);
    }

    @Override
    public String toString() {
        return String.format("t%02X", get());
    }
}
