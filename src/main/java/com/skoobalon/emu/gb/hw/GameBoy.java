package com.skoobalon.emu.gb.hw;

import com.google.common.eventbus.EventBus;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.rom.GameBoyRom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Steven Wallace
 */
public class GameBoy {
    private static final Logger logger = LoggerFactory.getLogger(GameBoy.class);

    private final EventBus bus;
    private GameBoyRom rom;
    private Cpu cpu;
    private Mmu mmu;

    public GameBoy(GameBoyRom rom) {
        this.bus = new EventBus();
        this.rom = rom;
        this.mmu = new Mmu(bus);
        this.cpu = new Cpu(rom, mmu);

        // First thing the Game Boy does is make the rom addressable
        mmu.setRom(rom);
    }

    public EventBus getBus() {
        return bus;
    }

    public GameBoyRom getRom() {
        return rom;
    }

    public Cpu getCpu() {
        return cpu;
    }

    public Mmu getMmu() {
        return mmu;
    }

}
