package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Flag;
import com.skoobalon.emu.gb.hw.cpu.Register8;

/**
 * Subtract with carry
 * @author Steven Wallace
 */
public class SBC {
    /**
     * Subtract source from destination and decrement destination if Carry flag was set prior to operation
     * The value in the source register is decremented from the value in the destination register, and an additional
     * value is decremented from the destination register if the carry flag was set prior to this operation being
     * called.
     *
     * Flags: Zero if result is zero, Operation (subtract), Carry if underflow occurred
     * m-Time: 1
     * OpCodes: 0x98-0x9D, 0x9F
     *
     * @param cpu
     * @param dest
     * @param src
     */
    public static void r_r(Cpu cpu, Register8 dest, Register8 src) {
        // Decrement src from dest, subtracting an additional value if carry flag is set
        int value = dest.getValue() - src.getValue() - (cpu.getRegisterF().isSet(Flag.Carry) ? 1 : 0);
        dest.set(value);

        cpu.getRegisterF().setFlags(value < 0, false, true, value == 0);
    }

    /**
     * Subtract value in memory pointed to by source registers from destination and decrement destination if Carry flag was set prior to operation
     * The value in memory pointed to by the source registers is decremented from the value in the source register, and
     * and additional value is decremented from the destination register if the carry flag was set prior to this
     * operation being called.
     *
     * Flags: Zero if result is zero, Operation (subtract), Carry if underflow occurred
     * m-Time: 2
     * OpCodes: 0x9E
     *
     * @param cpu
     * @param mmu
     * @param dest
     * @param src0
     * @param src1
     */
    public static void r_rr(Cpu cpu, Mmu mmu, Register8 dest, Register8 src0, Register8 src1) {
        // Decrement memory pointed to by src from dest, subtracting an additional value if carry flag is set
        int value = dest.getValue() - mmu.read8(src0, src1) - (cpu.getRegisterF().isSet(Flag.Carry) ? 1 : 0);

        cpu.getRegisterF().setFlags(value < 0, false, true, value == 0);
    }

    /**
     * Subtract next instruction from the destination register
     * OpCodes: 0xDE
     * @param cpu
     * @param dest
     */
    public static void r_n(Cpu cpu, Register8 dest) {
        int oldValue = dest.getValue();
        int sub = cpu.getNextOpCode();
        int newValue = oldValue - sub - (cpu.getRegisterF().isSet(Flag.Carry) ? 1 : 0);
        dest.set(newValue);
        cpu.getRegisterF().setFlags(newValue > 0xFF, ((oldValue ^ sub ^ dest.getValue()) & 0x10) == 0x10, true, dest.getValue() == 0);
    }
}
