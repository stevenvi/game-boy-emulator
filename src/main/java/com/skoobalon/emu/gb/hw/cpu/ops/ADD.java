package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Flag;
import com.skoobalon.emu.gb.hw.cpu.Register16;
import com.skoobalon.emu.gb.hw.cpu.Register8;

/**
 * @author Steven Wallace
 */
public class ADD {
    /**
     * OpCodes: 0x09, 0x19, 0x29
     * @param cpu
     * @param dest0
     * @param dest1
     * @param src0
     * @param src1
     */
    public static void rr_rr(Cpu cpu, Register8 dest0, Register8 dest1, Register8 src0, Register8 src1) {
        // Add as words
        int value = (dest0.getValue() << 8) + (src0.getValue() << 8) + dest1.getValue() + src1.getValue();

        // Mark as overflow if necessary
        if (value > 0xFFFF) {
            // Overflow occurred, mark it
            cpu.getRegisterF().addFlag(Flag.Carry);
        } else {
            // No overflow, make sure overflow bit is not set
            cpu.getRegisterF().removeFlag(Flag.Carry);
        }

        // Store value in destination registers
        dest0.set(value >> 8);
        dest1.set(value);
    }
    /**
     * Add source register into destination register
     * The value from the source register is added to the value in the destination register and stored in the
     * destination register.
     *
     * Flags: Set to contain Zero if result is zero (due to overflow) and/or Carry if result overflows
     * m-Time: 1
     * OpCodes: 0x80-0x85, 0x87
     * @param cpu
     * @param dest
     * @param src
     */
    public static void r_r(Cpu cpu, Register8 dest, Register8 src) {
        // Perform ADD operation
        dest.set(dest.getValue() + src.getValue());

        cpu.getRegisterF().setFlags(dest.getValue() < src.getValue(), false, false, dest.getValue() == 0);
    }

    /**
     * Add value from memory at source registers into destination registers
     * The value from memory pointed to by the source registers is added and stored into the value of the
     * destination register.
     *
     * Flags: Set to contain Zero if result is zero (due to overflow) and/or Carry if result overflows
     * m-Time: 2
     * OpCodes: 0x86
     * @param cpu
     * @param mmu
     * @param dest
     * @param src0
     * @param src1
     */
    public static void r_rr(Cpu cpu, Mmu mmu, Register8 dest, Register8 src0, Register8 src1) {
        // Perform ADD operation with result from MMU
        int src = mmu.read8(src0, src1);
        dest.set(dest.getValue() + src);

        // Update flags register
        cpu.getRegisterF().setFlags(dest.getValue() < src, false, false, dest.getValue() == 0);
    }

    /**
     * Adds 16-bit input register into the two 8-bit destination register pair, setting or unsetting the carry flag
     * as applicable.
     *
     * OpCodes: 0x39
     * @param cpu
     * @param dest0
     * @param dest1
     * @param src
     */
    public static void rr_R(Cpu cpu, Register8 dest0, Register8 dest1, Register16 src) {
        // Perform ADD operation
        int value = (dest0.getValue() << 8) + dest1.getValue() + src.getValue();

        // Set or unset carry flag as applicable
        cpu.getRegisterF().setFlag(Flag.Carry, value > 0xFFFF);

        // Store destination value
        dest0.set(value >> 8);
        dest1.set(value);
    }

    /**
     * Add the next byte from ROM into the destination register
     * OpCodes: 0xC6
     * @param cpu
     * @param dest
     */
    public static void r_n(Cpu cpu, Register8 dest) {
        // ADD NN
        int oldValue = dest.getValue();
        int add = cpu.getNextOpCode();
        int newValue = oldValue + add;
        dest.set(newValue);
        cpu.getRegisterF().setFlags(
                newValue != dest.getValue(),
                ((oldValue ^ add ^ dest.getValue()) & 0x10) != 0,
                false,
                dest.getValue() == 0);
    }

    /**
     * OpCodes: 0xE8
     * @param cpu
     * @param mmu
     */
    public static void SP_d(Cpu cpu, Mmu mmu) {
        int i = mmu.read8(cpu.getRegisterPC().getAndIncrement());
        if (i >= 0x80) {
            i = -((~i + 1) & 0xFF);
        }

        cpu.getRegisterSP().set(cpu.getRegisterSP().getValue() + i);
    }
}
