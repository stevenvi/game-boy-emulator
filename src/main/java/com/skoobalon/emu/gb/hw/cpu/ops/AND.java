package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Register8;

/**
 * @author Steven Wallace
 */
public class AND {
    /**
     * Perform binary AND between values in source and destination registers, storing result in destination register
     *
     * Flags: Zero if result is zero
     * m-Time: 1
     * OpCodes: 0xA0-0xA5, 0xA7
     *
     * @param cpu
     * @param dest
     * @param src
     */
    public static void r_r(Cpu cpu, Register8 dest, Register8 src) {
        // Perform AND
        dest.set(dest.getValue() & src.getValue());

        // Set zero flag and record time spent
        cpu.getRegisterF().setZero(dest.getValue() == 0);
    }

    /**
     * Performs binary AND between value in memory pointed to by the source registers and value in destination register,
     * storing the result in the destination register.
     *
     * Flags: Zero if result is zero
     * m-Time: 2
     * OpCodes: 0xA6
     *
     * @param cpu
     * @param mmu
     * @param dest
     * @param src0
     * @param src1
     */
    public static void r_rr(Cpu cpu, Mmu mmu, Register8 dest, Register8 src0, Register8 src1) {
        // Perform AND
        int value = dest.getValue() & mmu.read8(src0, src1);
        dest.set(value);

        // Set zero flag and record time spent
        cpu.getRegisterF().setZero(value == 0);
    }

    /**
     * OpCodes: 0xE6
     * @param cpu
     * @param dest
     */
    public static void r_n(Cpu cpu, Register8 dest) {
        int tempValue = cpu.getNextOpCode();
        dest.set(dest.getValue() & tempValue);
        cpu.getRegisterF().setFlags(false, true, false, dest.getValue() == 0);
    }
}
