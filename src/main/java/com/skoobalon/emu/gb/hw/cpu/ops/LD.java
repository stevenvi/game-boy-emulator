package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Register16;
import com.skoobalon.emu.gb.hw.cpu.Register8;

/**
 * @author Steven Wallace
 */
public class LD {
    /**
     * OpCodes: 0x01, 0x11, 0x21
     * @param cpu
     * @param pc
     * @param dest0
     * @param dest1
     */
    public static void LD_rr_nn(Cpu cpu, Register16 pc, Register8 dest0, Register8 dest1) {
        // Read program counter value into dest registers
        dest1.set(cpu.getNextOpCode());
        dest0.set(cpu.getNextOpCode());
    }

    /**
     * Load the next instruction into a register
     * OpCodes: 0x06, 0x0E, 0x16, 0x1E, 0x26, 0x2E, 0x3E
     * @param cpu
     * @param pc
     * @param dest
     */
    public static void LD_r_n(Cpu cpu, Register16 pc, Register8 dest) {
        dest.set(cpu.getNextOpCode());
    }

    /**
     * Write value from source register into destination memory address
     * The value stored in the source register is written to memory at the address pointed to by the destination
     * registers.
     *
     * Flags: Unmodified
     * m-Time: 2
     * OpCodes: 0x02, 0x12, 0x70-0x75, 0x77
     * @param cpu
     * @param mmu
     * @param dest0
     * @param dest1
     * @param src
     */
    public static void LD_rr_r(Cpu cpu, Mmu mmu, Register8 dest0, Register8 dest1, Register8 src) {
        // Write value from source register into destination memory address
        mmu.write8((dest0.getValue() << 8) + dest1.getValue(), src.getValue());
    }

    /**
     * Load value from source register into destination register
     * The value from the source register is copied into the destination register
     * Flags: Unmodified
     * m-Time: 1
     *
     * OpCodes: 0x40-0x45, 0x47-4D, 0x4F-0x55, 0x57-0x5D, 0x5F-0x65, 0x67-0x6D, 0x6F, 0x78-0x7D, 0x7F
     * @param cpu
     * @param dest
     * @param src
     */
    public static void LD_r_r(Cpu cpu, Register8 dest, Register8 src) {
        // Copy data from one register to the other
        dest.setValue(src.getValue());
    }

    /**
     * OpCodes: 0x08
     * @param cpu
     * @param mmu
     * @param r
     */
    public static void LD_nn_R(Cpu cpu, Mmu mmu, Register16 r) {
        int addr = cpu.getNextWord();
        mmu.write8(addr, r.getValue() >> 8);
        mmu.write8(addr + 1, r.getValue());
    }

    /**
     * Flags: Unmodified
     * m-Time: 2
     * OpCodes: 0x0A, 0x1A, 0x46, 0x4E, 0x56, 0x5E, 0x66, 0x6E
     * @param cpu
     * @param mmu
     * @param dest
     * @param src0
     * @param src1
     */
    public static void LD_r_rr(Cpu cpu, Mmu mmu, Register8 dest, Register8 src0, Register8 src1) {
        // Read value pointed to by src registers from memory and put into dest register
        dest.set(mmu.read8(src0, src1));
    }

    /**
     * Read value from memory into 16-bit register
     * Reads the value in memory pointed to by the next value on the program counter and stores it into the 16-bit
     * destination register.
     *
     * OpCodes: 0x31
     * @param cpu
     * @param pc
     * @param dest
     */
    public static void LD_R_nn(Cpu cpu, Register16 pc, Register16 dest) {
        dest.set(cpu.getNextWord());
    }

    /**
     * Read and increment value from PC and store at the specified memory position
     * Reads the value from memory pointed to by the next program instruction, then stores it into the memory
     * location pointed to by the destination registers.
     *
     * OpCodes: 0x36
     * @param cpu
     * @param mmu
     * @param dest0
     * @param dest1
     */
    public static void LD_rr_n(Cpu cpu, Mmu mmu, Register8 dest0, Register8 dest1) {
        int pc = cpu.getRegisterPC().getValue();
        mmu.write8(dest0, dest1, mmu.read8(cpu.getRegisterPC().getValue()));

        cpu.getRegisterPC().set(pc + 1);
    }

    /**
     * OpCodes: 0xEA
     * @param cpu
     * @param mmu
     * @param src
     */
    public static void LD_nn_r(Cpu cpu, Mmu mmu, Register8 src) {
        int pc = cpu.getRegisterPC().getValue();
        mmu.write8(mmu.read16(pc), src.getValue());
        cpu.getRegisterPC().set(pc + 2);
    }

    /**
     * Load value in source register into memory at destination word, then decrement destination word
     * Stores the value in the source register into the memory at the location pointed to by the destination
     * registers, then decrements the value of the destination registers as a word.
     *
     * OpCodes: 0x32
     * @param cpu
     * @param mmu
     * @param dest0
     * @param dest1
     * @param src
     */
    public static void LDD_rr_r(Cpu cpu, Mmu mmu, Register8 dest0, Register8 dest1, Register8 src) {
        // Write value from src register to dest memory location
        mmu.write8((dest0.getValue() << 8) + dest1.getValue(), src.getValue());

        // Decrement destination memory location
        dest1.set(dest1.getValue() - 1);
        if (dest1.getValue() == 0xFF) {
            dest0.set(dest0.getValue() - 1);
        }
    }

    /**
     * Load value from memory at source registers into destination register and decrement memory address
     * The value from memory pointed to by the source registers is loaded into the destination register, then the
     * source register word is decremented by 1.
     *
     * Flags: No flags are modified
     * m-Time: 2
     * OpCodes: 0x3A
     *
     * @param cpu
     * @param mmu
     * @param dest
     * @param src0
     * @param src1
     */
    public static void LDD_r_rr(Cpu cpu, Mmu mmu, Register8 dest, Register8 src0, Register8 src1) {
        // Read value from memory into destination register
        dest.set(mmu.read8(src0, src1));

        // Decrement memory location
        src1.set(src1.getValue() - 1);
        if (src1.getValue() == 255) {
            src0.set(src0.getValue() - 1);
        }
    }

    /**
     * OpCodes: 0xE0
     * @param cpu
     * @param mmu
     * @param src
     */
    public static void LDH_n_r(Cpu cpu, Mmu mmu, Register8 src) {
        int pc = cpu.getRegisterPC().getValue();
        mmu.write8(0xFF00 + mmu.read8(pc), src.getValue());
        cpu.getRegisterPC().set(pc + 1);
    }

    /**
     * OpCodes: 0xE2
     * @param cpu
     * @param mmu
     * @param dest
     * @param src
     */
    public static void LDH_mr_r(Cpu cpu, Mmu mmu, Register8 dest, Register8 src) {
        mmu.write8(0xFF00 + dest.getValue(), src.getValue());
    }

    /**
     * OpCodes: 0xF0
     * @param cpu
     * @param mmu
     * @param dest
     */
    public static void LDH_r_n(Cpu cpu, Mmu mmu, Register8 dest) {
        int pc = cpu.getRegisterPC().getValue();
        dest.set(mmu.read8(0xFF00 + mmu.read8(pc)));
        cpu.getRegisterPC().set(pc + 1);
    }

    /**
     * OpCodes: 0xF2
     * @param cpu
     * @param mmu
     * @param dest
     * @param src
     */
    public static void LDH_r_mr(Cpu cpu, Mmu mmu, Register8 dest, Register8 src) {
        dest.set(mmu.read8(0xFF00 + src.getValue()));
    }

    /**
     * Writes the contents of the source register into the memory address pointed to by the destination registers,
     * then increments the value of the destination registers by 1.
     *
     * OpCodes: 0x22
     * @param cpu
     * @param mmu
     * @param dest0
     * @param dest1
     * @param src
     */
    public static void LDI_rr_r(Cpu cpu, Mmu mmu, Register8 dest0, Register8 dest1, Register8 src) {
        // Put value of src register into memory at location in dest registers
        mmu.write8((dest0.getValue() << 8) + dest1.getValue(), src.getValue());

        // Increment value in dest registers
        dest1.set(dest1.getValue() + 1);
        if (dest1.getValue() == 0) {
            dest0.set(dest0.getValue() + 1);
        }
    }

    /**
     * Load value in memory pointed to by source register word into destination register, incrementing source register word
     *
     * OpCodes: 0x2A
     * @param cpu
     * @param mmu
     * @param dest
     * @param src0
     * @param src1
     */
    public static void LDI_r_rr(Cpu cpu, Mmu mmu, Register8 dest, Register8 src0, Register8 src1) {
        dest.set(mmu.read8(src0, src1));
        src1.set(src1.getValue() + 1);
        if (src1.getValue() == 0) {
            src0.set(src0.getValue() + 1);
        }
    }

    /**
     * OpCodes: 0xF8
     * @param cpu
     */
    public static void LD_HL_SP_nn(Cpu cpu) {
        // Read the next value from memory and treat as a signed value
        int offset = cpu.getNextOpCode();
        // If high bit is set, treat as a negative value
        if (offset >= 0x80) {
            offset = -((~offset + 1) & 0xFF);
        }

        int oldValue = cpu.getRegisterSP().getValue();
        int newValue = (oldValue + offset) & 0xFFFF;
        if(offset >= 0) {
            cpu.getRegisterF().setFlags(oldValue > newValue, ((oldValue ^ offset ^ newValue) & 0x1000) == 0x1000, false, false);
        } else {
            cpu.getRegisterF().setFlags(oldValue < newValue, ((oldValue ^ offset ^ newValue) & 0x1000) == 0x1000, false, false);
        }
        cpu.getRegisterH().set(newValue >> 8);
        cpu.getRegisterL().set(newValue);
    }

    /**
     * OpCodes: 0xF9
     * @param cpu
     */
    public static void LD_SP_HL(Cpu cpu) {
        cpu.getRegisterSP().set((cpu.getRegisterH().getValue() << 8) | cpu.getRegisterL().getValue());
    }

    /**
     * OpCodes: 0xFA
     * @param cpu
     * @param dest
     */
    public static void LD_r_nn(Cpu cpu, Mmu mmu, Register8 dest) {
        dest.set(mmu.read8(cpu.getNextWord()));
    }
}
