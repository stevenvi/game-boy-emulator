package com.skoobalon.emu.gb.hw.cpu;

/**
 * @author Steven Wallace
 */
public enum Flag {
    /**
     * Set if the last operation produced a result over 255 for adds or under 0 for subtracts
     */
    Carry(0x10),
    /**
     * Set if the last operation's lower half of byte overflowed past 0xF
     */
    HalfCarry(0x20),
    /**
     * Set if the last operation was a subtract
     */
    Operation(0x40),
    /**
     * Set if the last operation produced a result of 0
     */
    Zero(0x80);

    private final int mask;

    Flag(int mask) {
        this.mask = mask;
    }

    public int getMask() {
        return mask;
    }

}
