package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Flag;

/**
 * @author Steven Wallace
 */
public final class RET {
    /**
     * OpCodes: 0xC0, 0xC8, 0xD0, 0xD8
     * @param cpu
     * @param mmu
     */
    public static void conditional(Cpu cpu, Mmu mmu, Flag flag, boolean flagSet) {
        if (cpu.getRegisterF().isSet(flag) == flagSet) {
            // Flag is in correct state
            int sp = cpu.getRegisterSP().getValue();
            cpu.getRegisterPC().set(mmu.read16(sp));
            cpu.getRegisterSP().set(sp + 2);
            cpu.incrementM(2);
        } else {
            // Flag is not in correct state, carry on
        }
    }

    /**
     * OpCodes: 0xC9
     * @param cpu
     * @param mmu
     */
    public static void ret(Cpu cpu, Mmu mmu) {
        int sp = cpu.getRegisterSP().getValue();
        cpu.getRegisterPC().set(mmu.read16(sp));
        cpu.getRegisterSP().set(sp + 2);
    }

    public static void reti(Cpu cpu, Mmu mmu) {
        int sp = cpu.getRegisterSP().getValue();
        cpu.setIme(true);
        cpu.getRegisterPC().set(mmu.read16(sp));
        cpu.getRegisterSP().set(sp + 2);
    }
}
