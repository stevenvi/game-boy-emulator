package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Register16;
import com.skoobalon.emu.gb.hw.cpu.Register8;

/**
 * Decrement
 * @author Steven Wallace
 */
public class DEC {
    /**
     * OpCodes: 0x05, 0x0D, 0x15, 0x1D, 0x25, 0x2D, 0x3D
     * @param cpu
     * @param register
     */
    public static void r(Cpu cpu, Register8 register) {
        register.set(register.getValue() - 1);
        cpu.getRegisterF().setZero(register.getValue() == 0);
    }

    /**
     * OpCodes: 0x0B, 0x1B, 0x2B
     * @param cpu
     * @param r0
     * @param r1
     */
    public static void rr(Cpu cpu, Register8 r0, Register8 r1) {
        // Decrement low bit
        r1.set(r1.getValue() - 1);

        // If low bit wrapped, decrement high bit, too
        if (r1.getValue() == 0xFF) {
            r0.set(r0.getValue() - 1);
        }
    }

    /**
     * Decrement value in memory pointed to by input registers
     *
     * OpCodes: 0x35
     * @param cpu
     * @param mmu
     * @param r0
     * @param r1
     */
    public static void rr(Cpu cpu, Mmu mmu, Register8 r0, Register8 r1) {
        int addr = (r0.getValue() << 8) + r1.getValue();
        int i = mmu.read8(addr) - 1;
        mmu.write8(addr, i);

        cpu.getRegisterF().setZero(i == 0);
    }

    /**
     * Decrements the value in the input register
     * Flags: Unmodified
     * m-Time: 1
     * OpCodes: 0x3B
     * @param cpu
     * @param register
     */
    public static void R(Cpu cpu, Register16 register) {
        register.set(register.getValue() - 1);
    }
}
