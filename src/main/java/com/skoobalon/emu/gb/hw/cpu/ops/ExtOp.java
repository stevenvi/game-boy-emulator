package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Flag;
import com.skoobalon.emu.gb.hw.cpu.Register8;
import com.skoobalon.emu.gb.hw.cpu.RegisterFlags;

/**
 * Two byte opcodes, grouped in a single class due to their repetitive nature.
 * @author Steven Wallace
 */
public final class ExtOp {

    private static int getAddr(Register8 mem0, Register8 mem1) {
        return (mem0.getValue() << 8) + mem1.getValue();
    }

    private static int RLC(RegisterFlags flags, int value) {
        flags.setFlags((value & 0x80) == 0x80, false, false, value == 0);
        return (value << 1) | (value >> 7);
    }

    /**
     * Rotate left with carry
     * OpCodes: 0xCB00-0xCB05, 0xCB07
     * @param cpu
     * @param r
     */
    public static void RLC_r(Cpu cpu, Register8 r) {
        r.set(RLC(cpu.getRegisterF(), r.getValue()));
    }

    /**
     * OpCodes: 0xCB06
     * @param cpu
     * @param mmu
     * @param mem0
     * @param mem1
     */
    public static void RLC_rr(Cpu cpu, Mmu mmu, Register8 mem0, Register8 mem1) {
        int addr = getAddr(mem0, mem1);
        mmu.write8(addr, RLC(cpu.getRegisterF(), mmu.read8(addr)));
    }


    private static int RRC(RegisterFlags flags, int value) {
        flags.setFlags((value & 0x01) == 0x01, false, false, value == 0);
        return (value >> 1) | (value << 7);
    }

    /**
     * Rotate right with carry
     * OpCodes: 0xCB08-0xCB0D, 0xCB0F
     * @param cpu
     * @param r
     */
    public static void RRC_r(Cpu cpu, Register8 r) {
        r.set(RRC(cpu.getRegisterF(), r.getValue()));
    }

    /**
     * Rotate right with carry
     * OpCodes: 0xCB0E
     * @param cpu
     * @param mmu
     * @param mem0
     * @param mem1
     */
    public static void RRC_rr(Cpu cpu, Mmu mmu, Register8 mem0, Register8 mem1) {
        int addr = getAddr(mem0, mem1);
        mmu.write8(addr, RRC(cpu.getRegisterF(), mmu.read8(addr)));
    }

    private static int RL(RegisterFlags flags, int value) {
        value = (value << 1) | (flags.isSet(Flag.Carry) ? 1 : 0);
        flags.setFlags((value & 0x100) == 0x100, false, false, value == 0);
        return value;
    }

    /**
     * Rotate left add 1 if carry was set
     * OpCodes: 0xCB10-0xCB15, 0xCB17
     * @param cpu
     * @param r
     */
    public static void RL_r(Cpu cpu, Register8 r) {
        r.set(RL(cpu.getRegisterF(), r.getValue()));
    }

    /**
     * OpCodes: 0xCB16
     * @param cpu
     * @param mmu
     * @param mem0
     * @param mem1
     */
    public static void RL_rr(Cpu cpu, Mmu mmu, Register8 mem0, Register8 mem1) {
        int addr = getAddr(mem0, mem1);
        mmu.write8(addr, RL(cpu.getRegisterF(), mmu.read8(addr)));
    }

    private static int RR(RegisterFlags flags, int value) {
        boolean setCarry = (value & 0x01) == 0x01;
        value = (value >> 1) | (flags.isSet(Flag.Carry) ? 0x80 : 0);
        flags.setFlags(setCarry, false, false, value == 0);
        return value;
    }

    /**
     * OpCodes: 0xCB18-0xCB1D, 0xCB1F
     * @param cpu
     * @param r
     */
    public static void RR_r(Cpu cpu, Register8 r) {
        r.set(RR(cpu.getRegisterF(), r.getValue()));
    }

    /**
     * OpCodes: 0xCB1E
     * @param cpu
     * @param mmu
     * @param mem0
     * @param mem1
     */
    public static void RR_rr(Cpu cpu, Mmu mmu, Register8 mem0, Register8 mem1) {
        int addr = getAddr(mem0, mem1);
        mmu.write8(addr, RR(cpu.getRegisterF(), mmu.read8(addr)));
    }

    private static int SLA(RegisterFlags flags, int value) {
        boolean carry = (value & 0x80) == 0x80;
        value <<= 1;
        flags.setFlags(carry, false, false, value == 0);
        return value;
    }

    public static void SLA_r(Cpu cpu, Register8 r) {
        r.set(SLA(cpu.getRegisterF(), r.getValue()));
    }

    public static void SLA_rr(Cpu cpu, Mmu mmu, Register8 mem0, Register8 mem1) {
        int addr = getAddr(mem0, mem1);
        mmu.write8(addr, SLA(cpu.getRegisterF(), mmu.read8(addr)));
    }

    private static int SRA(RegisterFlags flags, int value) {
        flags.setFlags((value & 0x1) == 0x1, false, false, value == 0x1);
        return value >> 1;
    }

    public static void SRA_r(Cpu cpu, Register8 r) {
        r.set(SRA(cpu.getRegisterF(), r.getValue()));
    }

    public static void SRA_rr(Cpu cpu, Mmu mmu, Register8 mem0, Register8 mem1) {
        int addr = getAddr(mem0, mem1);
        mmu.write8(addr, SRA(cpu.getRegisterF(), mmu.read8(addr)));
    }

    private static int SWAP(RegisterFlags flags, int value) {
        flags.setFlag(Flag.Zero, value == 0);
        return (value >> 4) | (value << 4);
    }

    /**
     * Swap high and low nibbles
     * OpCodes: 0xCB30-0xCB35, 0xCD37
     * @param cpu
     * @param r
     */
    public static void SWAP_r(Cpu cpu, Register8 r) {
        r.set(SWAP(cpu.getRegisterF(), r.getValue()));
    }

    /**
     * Swap high and low nibbles
     * OpCodes: 0xCB36
     * @param cpu
     * @param mmu
     * @param mem0
     * @param mem1
     */
    public static void SWAP_rr(Cpu cpu, Mmu mmu, Register8 mem0, Register8 mem1) {
        int addr = getAddr(mem0, mem1);
        mmu.write8(addr, SWAP(cpu.getRegisterF(), mmu.read8(addr)));
    }

    private static int SRL(RegisterFlags flags, int value) {
        flags.setFlags((value & 0x01) == 0x01, false, false, value == 1);
        return value >> 1;
    }

    public static final void SRL_r(Cpu cpu, Register8 r) {
        r.set(SRL(cpu.getRegisterF(), r.getValue()));
    }

    public static final void SRL_rr(Cpu cpu, Mmu mmu, Register8 mem0, Register8 mem1) {
        int addr = getAddr(mem0, mem1);
        mmu.write8(addr, SRL(cpu.getRegisterF(), mmu.read8(addr)));
    }

    private static void BIT(RegisterFlags flags, int bit, int value) {
        flags.setFlags(
                flags.isSet(Flag.Carry),                    // Retain carry flag
                true,                                       // Set half-carry flag
                false,                                      // Clear operation flag
                (value & (0x1 << bit)) == 0);               // Set zero if bit is clear
    }

    /**
     * OpCodes: 0xCB40-0xCB7F
     * @param cpu
     * @param bit
     * @param r
     */
    public static void BIT_r(Cpu cpu, int bit, Register8 r) {
        BIT(cpu.getRegisterF(), bit, r.getValue());
    }

    /**
     * OpCodes: 0xCB40-0xCB7F
     * @param cpu
     * @param mmu
     * @param bit
     * @param mem0
     * @param mem1
     */
    public static void BIT_rr(Cpu cpu, Mmu mmu, int bit, Register8 mem0, Register8 mem1) {
        BIT(cpu.getRegisterF(), bit, mmu.read8(mem0, mem1));
    }

    private static int RES(int bit, int value) {
        return value & (~(0x1 << bit));
    }

    public static void RES_r(Cpu cpu, int bit, Register8 r) {
        r.set(RES(bit, r.getValue()));
    }

    public static void RES_rr(Cpu cpu, Mmu mmu, int bit, Register8 mem0, Register8 mem1) {
        int addr = getAddr(mem0, mem1);
        mmu.write8(addr, RES(bit, mmu.read8(addr)));
    }

    private static int SET(int bit, int value) {
        return value | (0x1 << bit);
    }

    public static void SET_r(Cpu cpu, int bit, Register8 r) {
        r.set(SET(bit, r.getValue()));
    }

    public static void SET_rr(Cpu cpu, Mmu mmu, int bit, Register8 mem0, Register8 mem1) {
        int addr = (mem0.getValue() << 8) + mem1.getValue();
        mmu.write8(addr, SET(bit, mmu.read8(addr)));
    }
}
