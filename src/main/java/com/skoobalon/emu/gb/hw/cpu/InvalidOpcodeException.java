package com.skoobalon.emu.gb.hw.cpu;

/**
 * @author Steven Wallace
 */
public class InvalidOpcodeException extends RuntimeException {
    public InvalidOpcodeException(String msg) {
        super(msg);
    }
}
