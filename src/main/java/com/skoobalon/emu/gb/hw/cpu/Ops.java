package com.skoobalon.emu.gb.hw.cpu;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.events.HaltEvent;

/**
 * All CPU operations
 * @author Steven Wallace
 */
public final class Ops {

    /**
     * OpCodes: 0x00
     * @param cpu
     */
    public static void NOOP(Cpu cpu) {
        // (Does nothing)
    }

    /**
     * OpCodes: 0x07
     * @param cpu
     * @param register
     */
    public static void RLC_r(Cpu cpu, Register8 register) {
        // Check if high bit is set on unmutated value
        if ((register.getValue() & 0x80) != 0) {
            // High bit is set
            // Left shift value, moving overflow bit back to the lowest position
            register.set((register.getValue() << 1) + 1);

            // Make sure that the carry flag is set, but don't change any other flags
            cpu.getRegisterF().addFlag(Flag.Carry);
        } else {
            // High bit is not set
            // Left shift value
            register.set(register.getValue() << 1);

            // Make sure that the carry flag is NOT set
            cpu.getRegisterF().removeFlag(Flag.Carry);
        }
    }

    /**
     * OpCodes: 0x17
     * @param cpu
     * @param register
     */
    public static void RL_r(Cpu cpu, Register8 register) {
        int prevOpCarry = cpu.getRegisterF().isSet(Flag.Carry) ? 1 : 0;
        boolean highBitSet = (register.getValue() & 0x80) == 0x80;

        // Shift left, wrapping if previous operation involved a carry
        register.set((register.getValue() << 1) + prevOpCarry);
        cpu.getRegisterF().setFlag(Flag.Carry, highBitSet);
    }

    /**
     * Right shifts a register by 1 bit, wrapping low bit around to the high bit.
     * The overflow bit is touched, while others remain unmodified.
     * OpCodes: 0x0F
     * @param cpu
     * @param register
     */
    public static void RRC_r(Cpu cpu, Register8 register) {
        // Check if low bit is set on unmutated value
        if ((register.getValue() & 0x1) != 0) {
            // Low bit is set
            // Right shift value, moving overflow bit up to the highest position
            register.set((register.getValue() >> 1) + 0x80);

            // Make sure that the carry flag is set, but don't change any other flags
            cpu.getRegisterF().addFlag(Flag.Carry);
        } else {
            // Low bit is not set
            // Right shift value
            register.set(register.getValue() >> 1);

            // Make sure that the carry flag is NOT set
            cpu.getRegisterF().removeFlag(Flag.Carry);
        }
    }

    /**
     * OpCodes: 0x1F
     * @param cpu
     * @param register
     */
    public static void RR_r(Cpu cpu, Register8 register) {
        int prevOpCarry = cpu.getRegisterF().isSet(Flag.Carry) ? 0x80 : 0;
        boolean lowBitSet = (register.getValue() & 0x1) == 0x1;

        // Shift right, wrapping if previous operation involved a carry
        register.set((register.getValue() >> 1) + prevOpCarry);
        cpu.getRegisterF().setFlag(Flag.Carry, lowBitSet);
    }

    /**
     * TODO: I have no idea
     * OpCodes: 0x10
     * @param cpu
     * @param mmu
     */
    public static void STOP(Cpu cpu, Mmu mmu) {
        int pc = cpu.getRegisterPC().getValue();
        int i = mmu.read8(pc);
        if (i >= 0x80) {
            i = -((~i + 1) & 0xFF);
        }

        int b = cpu.getRegisterB().getValue() - 1;
        cpu.getRegisterB().set(b);

        if (b == 0) {
            cpu.getRegisterPC().set(pc + 1);
        } else {
            cpu.getRegisterPC().set(pc + 1 + i);
        }
    }

    /**
     * Flip all bits in register A
     * All bits in register A are flipped. The subtract flag is set and the zero flag is set if the result is zero.
     *
     * OpCodes: 0x2F
     * @param cpu
     */
    public static void CPL(Cpu cpu) {
        // Flip all bits in register A
        Register8 r = cpu.getRegisterA();
        int value = (~r.getValue()) & 0xFF;
        r.set(value);

        // Set flags and record time
        cpu.getRegisterF().setFlags(false, false, true, value == 0);
    }

    /**
     * Sets the "Carry" flag, leaving others unmodified
     * OpCodes: 0x37
     * @param cpu
     */
    public static void SCF(Cpu cpu) {
        cpu.getRegisterF().addFlag(Flag.Carry);
    }

    /**
     * Toggle Carry flag state
     * If the carry flag is set, it is cleared. If it is clear, it is set
     * Flags: The carry flag's state is flipped
     * m-Time: 1
     * OpCodes: 0x3F
     * @param cpu
     */
    public static void CCF(Cpu cpu) {
        // Flip carry bit
        cpu.getRegisterF().flip(Flag.Carry);
    }

    public static void RST(Cpu cpu, Mmu mmu, int pcValue) {
        Register16 sp = cpu.getRegisterSP();
        Register16 pc = cpu.getRegisterPC();

        sp.set(sp.getValue() - 2);
        mmu.write16(sp.getValue(), pc.getValue());
        pc.set(pcValue);
    }

    /**
     * Halts execution. Users of the CPU have the option of ignoring this, so we'll just send along a HaltEvent and
     * the driver can handle it from there.
     *
     * @param cpu
     */
    public static void HALT(Cpu cpu) {
        cpu.getEventBus().post(new HaltEvent());
    }

    /**
     * Pops word off stack into input registers
     *
     * Flags: Unmodified
     * m-Time: 3
     * OpCodes: 0xC1, 0xD1, 0xE1, 0xF1
     *
     * @param cpu
     * @param mmu
     * @param r0
     * @param r1
     */
    public static void POP_rr(Cpu cpu, Mmu mmu, Register8 r0, Register8 r1) {
        // Pop value off stack and into the input register word
        int sp = cpu.getRegisterSP().getValue();
        r1.set(mmu.read8(sp));
        r0.set(mmu.read8(sp + 1));
        cpu.getRegisterSP().set(sp + 2);
    }

    /**
     * Pushed word in input registers onto the stack
     *
     * Flags: Unmodified
     * m-Time: 3
     * OpCodes: 0xC5, 0xD5, 0xE5, 0xF5
     *
     * @param cpu
     * @param mmu
     * @param r0
     * @param r1
     */
    public static void PUSH_rr(Cpu cpu, Mmu mmu, Register8 r0, Register8 r1) {
        // Push value from register word into the stack
        int sp = cpu.getRegisterSP().getValue();
        mmu.write8(sp - 1, r0.getValue());
        mmu.write8(sp - 2, r1.getValue());
        cpu.getRegisterSP().set(sp - 2);
    }

    /**
     * Disable interrupts
     * OpCodes: 0xF3
     * @param cpu
     */
    public static void DI(Cpu cpu) {
        // TODO: ??
        cpu.setIme(false);
    }

    /**
     * Enable interrupts
     * OpCodes: 0xFB
     * @param cpu
     */
    public static void EI(Cpu cpu) {
        cpu.setIme(true);
    }

    /**
     * Decimal adjust accumulator
     * Converts accumulator value to the decimal representation when stored in hex -- I think!
     * OpCodes: 0x27
     * @param cpu
     */
    public static void DAA(Cpu cpu) {
        // NOTE: Is this just converting from base16 to base10??
        // http://www.z80.info/z80syntx.htm#DAA

        // Adapted from cinoop emulator, is this accurate?!
        // https://github.com/CTurt/Cinoop/blob/master/source/cpu.c
        int a = cpu.getRegisterA().getValue();
        RegisterFlags f = cpu.getRegisterF();

        if (f.isSet(Flag.Operation)) {
            if (f.isSet(Flag.HalfCarry)) {
                a = (a - 0x06) & 0xFF;
            }
            if (f.isSet(Flag.Carry)) {
                a -= 0x60;
            }
        } else {
            if (f.isSet(Flag.HalfCarry) || (a & 0xF) > 9) {
                a += 0x06;
            }
            if (f.isSet(Flag.Carry) || a > 0x9F) {
                a += 0x60;
            }
        }

        // Store data, set flags, and log time
        cpu.getRegisterA().set(a);
        f.setFlags(a >= 0x100, false, f.isSet(Flag.Operation), a == 0);
    }

}
