package com.skoobalon.emu.gb.hw.cpu;

import com.google.common.eventbus.EventBus;
import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.ops.Operation;
import com.skoobalon.emu.gb.rom.GameBoyRom;
import com.skoobalon.emu.gb.util.HexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Steven Wallace
 */
public class Cpu {
    private static final Logger logger = LoggerFactory.getLogger(Cpu.class);

    // Used for notifying others of actions that happen on this CPU
    private final EventBus eventBus = new EventBus();

    // Flags
    boolean halt = false;
    boolean ime = false;

    GameBoyRom rom;

    // 8-bit registers
    private Register8 registerA = new Register8();
    private Register8 registerB = new Register8();
    private Register8 registerC = new Register8();
    private Register8 registerD = new Register8();
    private Register8 registerE = new Register8();
    private RegisterFlags registerF = new RegisterFlags();
    private Register8 registerH = new Register8();
    private Register8 registerL = new Register8();

    // 16-bit registers
    private Register16 registerPC = new Register16();
    private Register16 registerSP = new Register16();

    // Total execution time counter
    private int m = 0;

    private final Mmu mmu;

    public Cpu(GameBoyRom rom, Mmu mmu) {
        this.rom = rom;
        this.mmu = mmu;
        reset();
    }

    public void reset() {
        // Set initial states for GB
        getRegisterPC().set(0x0100);
        getRegisterSP().set(0xFFFE);

        // The default values for these are probably not important, and likely just what was left
        // after the scrolling Nintendo logo drops down.
        getRegisterA().set(0x01);
        getRegisterB().set(0x00);
        getRegisterC().set(0x13);
        getRegisterD().set(0x00);
        getRegisterE().set(0xD8);
        getRegisterF().setFlags(true, true, false, true);
        getRegisterH().set(0x01);
        getRegisterL().set(0x4D);
    }

    public int getNextOpCode() {
        return rom.readByteAt(registerPC.getAndIncrement());
    }

    public int getNextWord() {
        return getNextOpCode() | (getNextOpCode() << 8);
    }

    public void executeOpCode(int opCode) {
        Operation op = Operation.byOpCode(opCode);
        if (op == null) {
            throw new InvalidOpcodeException("Opcode " + HexUtil.toHexString8(opCode) + " not found!");
        } else {
            op.getOperation().accept(this, mmu);
            m += op.getMinClockCycles();
        }
    }

    public boolean getIme() {
        return ime;
    }

    public void setIme(boolean ime) {
        this.ime = ime;
    }

    public Register8 getRegisterA() {
        return registerA;
    }

    public Register8 getRegisterB() {
        return registerB;
    }

    public Register8 getRegisterC() {
        return registerC;
    }

    public Register8 getRegisterD() {
        return registerD;
    }

    public Register8 getRegisterE() {
        return registerE;
    }

    public RegisterFlags getRegisterF() {
        return registerF;
    }

    public Register8 getRegisterH() {
        return registerH;
    }

    public Register8 getRegisterL() {
        return registerL;
    }

    public Register16 getRegisterPC() {
        return registerPC;
    }

    public Register16 getRegisterSP() {
        return registerSP;
    }

    public int getM() {
        return m;
    }

    public void incrementM(int increment) {
        m += increment;
    }

    public EventBus getEventBus() {
        return eventBus;
    }
}
