package com.skoobalon.emu.gb.hw.cpu;

/**
 * @author Steven Wallace
 */
public class RegisterFlags extends Register8 {
    @Override
    public void set(int value) {
        // Only high 4 bits should ever be set on this register
        super.set(value & 0xF0);
    }

    public boolean isSet(Flag flag) {
        return (get() & flag.getMask()) == flag.getMask();
    }

    public void flip(Flag flag) {
        // TODO: Can we do this better/cleaner?
        int ci = (get() & flag.getMask()) != 0 ? 0 : flag.getMask();
        set(get() & (~flag.getMask()) + ci);
    }

    public void setFlags(boolean carry, boolean halfCarry, boolean operation, boolean zero) {
        set((carry ? Flag.Carry.getMask() : 0)
                | (halfCarry ? Flag.HalfCarry.getMask() : 0)
                | (operation ? Flag.Operation.getMask() : 0)
                | (zero ? Flag.Zero.getMask() : 0));
    }

    public void setZero(boolean zero) {
        set(zero ? Flag.Zero.getMask() : 0);
    }

    public void addFlag(Flag flag) {
        set(getValue() | flag.getMask());
    }

    public void removeFlag(Flag flag) {
        set(getValue() & ~flag.getMask());
    }

    public void setFlag(Flag flag, boolean set) {
        if (set) {
            addFlag(flag);
        } else {
            removeFlag(flag);
        }
    }

}
