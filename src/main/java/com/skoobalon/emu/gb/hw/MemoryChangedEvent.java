package com.skoobalon.emu.gb.hw;

/**
 * @author Steven Wallace
 */
public class MemoryChangedEvent {

    private final int address;
    private final int oldValue;
    private final int newValue;

    public MemoryChangedEvent(int address, int oldValue, int newValue) {
        this.address = address;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public int getAddress() {
        return address;
    }

    public int getOldValue() {
        return oldValue;
    }

    public int getNewValue() {
        return newValue;
    }
}
