package com.skoobalon.emu.gb.hw.cpu.events;

/**
 * Denotes that the CPU has halted
 * @author Steven Wallace
 */
public class HaltEvent {
}
