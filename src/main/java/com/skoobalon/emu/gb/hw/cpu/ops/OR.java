package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Register8;

/**
 * @author Steven Wallace
 */
public class OR {
    /**
     * Binary OR between values in src and dest registers, stored in dest register
     *
     * Flags: Zero if result is zero
     * m-Time: 1
     * OpCodes: 0xB0-0xB5, 0xB7
     *
     * @param cpu
     * @param dest
     * @param src
     */
    public static void r_r(Cpu cpu, Register8 dest, Register8 src) {
        dest.set(dest.getValue() | src.getValue());
        cpu.getRegisterF().setZero(dest.getValue() == 0);
    }

    /**
     * Binary OR between value in memory pointed to by src registers and dest register, stored in dest register
     *
     * Flags: Zero if result is zero
     * m-Time: 2
     * OpCodes: 0xB6
     *
     * @param cpu
     * @param mmu
     * @param dest
     * @param src0
     * @param src1
     */
    public static void r_rr(Cpu cpu, Mmu mmu, Register8 dest, Register8 src0, Register8 src1) {
        dest.set(dest.getValue() | mmu.read8(src0, src1));
        cpu.getRegisterF().setZero(dest.getValue() == 0);
    }

    /**
     * OpCodes: 0xF6
     * @param cpu
     */
    public static void n(Cpu cpu, Register8 dest) {
        int tempValue = cpu.getNextOpCode();
        dest.set(dest.getValue() | tempValue);
        cpu.getRegisterF().setZero(dest.getValue() == 0);
    }
}
