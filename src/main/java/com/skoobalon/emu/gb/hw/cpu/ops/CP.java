package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Register8;

/**
 * Compare
 * @author Steven Wallace
 */
public class CP {
    /**
     * Compares register A against the input register
     *
     * Flags: Zero if values in both registers are equal, Operation, and Carry if input value is larger than that in register A
     * m-Time: 1
     * OpCodes: 0xB8-0xBD, 0xBF
     *
     * @param cpu
     * @param r
     */
    public static void r(Cpu cpu, Register8 r) {
        int i = cpu.getRegisterA().getValue() - r.getValue();

        // Set Carry flag if value in input is larger than that in register A, or Zero if they're equal
        cpu.getRegisterF().setFlags(i < 0, false, true, i == 0);
    }

    /**
     * Compares register A against value in memory pointed to by input registers
     *
     * Flags: Zero if values are equal, Operation, and Carry if value in memory is larger than value in register A
     * m-Time: 2
     * OpCodes: 0xBE
     *
     * @param cpu
     * @param mmu
     * @param r0
     * @param r1
     */
    public static void rr(Cpu cpu, Mmu mmu, Register8 r0, Register8 r1) {
        int i = cpu.getRegisterA().getValue() - mmu.read8(r0, r1);

        // Set Carry flag if value in input is larger than that in register A, or Zero if they're equal
        cpu.getRegisterF().setFlags(i < 0, false, true, i == 0);
    }

    /**
     * OpCodes: 0xFE
     * @param cpu
     */
    public static void n(Cpu cpu) {
        // CP NN
        int nn = cpu.getNextOpCode();
        int computedValue = cpu.getRegisterA().getValue() - nn;
        int computedValueLowByte = computedValue & 0xFF;
        cpu.getRegisterF().setFlags(
                computedValue > 0xFF,
                ((cpu.getRegisterA().getValue() ^ nn ^ computedValueLowByte) & 0x10) == 0x10,
                true,
                computedValueLowByte == 0
        );
    }
}
