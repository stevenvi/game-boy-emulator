package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.Mmu;
import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Flag;
import com.skoobalon.emu.gb.hw.cpu.Register16;

/**
 * @author Steven Wallace
 */
public final class CALL {
    private static void call(Cpu cpu, Mmu mmu, Register16 pc, Register16 sp) {
        int b0 = cpu.getNextOpCode();
        int b1 = cpu.getNextOpCode();
        mmu.write8(sp.decrementAndGet(), pc.getValue() >> 8);
        mmu.write8(sp.decrementAndGet(), pc.getValue() & 0xFF);
        pc.set((b1 << 8) | b0);
    }

    /**
     * Calls value pointed to on PC if flag is in desired state
     * OpCodes: 0xC4, 0xCC, 0xD4, 0xDC
     * @param cpu
     * @param mmu
     * @param flag
     * @param flagSet
     */
    public static void conditional(Cpu cpu, Mmu mmu, Flag flag, boolean flagSet) {
        if (cpu.getRegisterF().isSet(flag) == flagSet) {
            call(cpu, mmu, cpu.getRegisterPC(), cpu.getRegisterSP());
            cpu.incrementM(3);
        } else {
            cpu.getRegisterPC().set(cpu.getRegisterPC().getValue() + 2);
        }
    }

    /**
     * Calls value pointed to on PC
     * OpCodes: 0xCD
     * @param cpu
     * @param mmu
     */
    public static void nn(Cpu cpu, Mmu mmu) {
        call(cpu, mmu, cpu.getRegisterPC(), cpu.getRegisterSP());
    }
}
