package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Flag;
import com.skoobalon.emu.gb.hw.cpu.Register16;
import com.skoobalon.emu.gb.hw.cpu.Register8;

/**
 * Jump
 * @author Steven Wallace
 */
public final class JP {

    /**
     * Jump to position at next instruction
     * Sets the program counter to the value in memory pointed to by the next instruction on the program counter.
     *
     * Flags: Unmodified
     * m-Time: 3
     * OpCodes: 0xC3
     *
     * @param cpu
     */
    public static void nn(Cpu cpu, Register16 pc) {
        pc.set(cpu.getNextWord());
    }

    /**
     * Jump to position at next instruction if input flag is in input state, otherwise skips over jump instruction
     * Sets the program counter to the value in memory pointed to by the next instruction on the program counter if the
     * input flag is in the input state prior to this instruction being called, otherwise the jump instruction is
     * skipped.
     *
     * Flags: Unmodified
     * m-Time: 4 if jump occurs, 3 otherwise
     * OpCodes: 0xC2, 0xCA, 0xD2, 0xDA
     *
     * @param cpu
     * @param pc
     * @param flag
     * @param flagSet
     */
    public static void nn_conditional(Cpu cpu, Register16 pc, Flag flag, boolean flagSet) {
        if (cpu.getRegisterF().isSet(flag) == flagSet) {
            // Flag state matches requirement, jump
            pc.set(cpu.getNextWord());
            cpu.incrementM(1);
        } else {
            // Flag state is incorrect, do not jump
            pc.set(pc.getValue() + 2);
        }
    }

    /**
     * Jump to program location referenced by input registers as word
     *
     * Flags: Unmodified
     * m-Time: 1
     * OpCodes: 0xE9
     * @param cpu
     * @param r0
     * @param r1
     */
    public static void rr(Cpu cpu, Register8 r0, Register8 r1) {
        cpu.getRegisterPC().set((r0.getValue() << 8) + r1.getValue());
    }
}
