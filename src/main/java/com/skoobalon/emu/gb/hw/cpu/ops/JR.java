package com.skoobalon.emu.gb.hw.cpu.ops;

import com.skoobalon.emu.gb.hw.cpu.Cpu;
import com.skoobalon.emu.gb.hw.cpu.Flag;
import com.skoobalon.emu.gb.hw.cpu.Register16;

/**
 * Jump relative to current position
 * @author Steven Wallace
 */
public class JR {
    /**
     * Increment the program counter by the signed value in memory pointed to by the next instruction
     * OpCodes: 0x18
     * @param cpu
     * @param pc
     */
    public static void n(Cpu cpu, Register16 pc) {
        // Read the next value from memory and treat as a signed value
        int i = cpu.getNextOpCode();
        // If high bit is set, treat as a negative value
        if (i >= 0x80) {
            i = -((~i + 1) & 0xFF);
        }

        // Jump and record time
        pc.set(pc.getValue() + i);
    }

    /**
     * Jump if the last operation set the flag to the specified state
     * Increments the program counter by the signed value in memory pointed to by the next instruction only if the
     * previous operation resulted in the input flag being set to the input state.
     *
     * OpCodes: 0x20
     * @param cpu
     * @param pc
     * @param flag
     * @param flagState
     */
    public static void n_conditional(Cpu cpu, Register16 pc, Flag flag, boolean flagState) {
        if (cpu.getRegisterF().isSet(flag) == flagState) {
            // Last operation was nonzero, jump
            // Read the next value from the program as a signed byte
            int i = cpu.getNextOpCode();
            if (i >= 0x80) {
                i = -((~i + 1) & 0xFF);
            }

            pc.set(pc.getValue() + i);
        } else {
            // Do not jump, skip over that instruction instead
            pc.incrementAndGet();
        }
    }

}
